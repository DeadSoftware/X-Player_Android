package org.alex.xplayer;

// Основной класс проекта XPlayer

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Window;
import android.view.MotionEvent;
import android.view.KeyEvent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Paint.Align;
//import android.widget.Toast;

public class Xplay extends Activity {
	
	private static Thread loading;
	//Создаём основной видимый объект типа CanvasView (описан в соответствующем файле)
	public static CanvasView xview;
	//Для безопасной перерисовки объекта из другого процесса, создаём свою команду repaint
	public static Runnable repaint = new Runnable() { public void run() { xview.invalidate(); }};
	
    /** Called when the activity is first created. */
	@Override
    //При создании активности программы вставляем на всё окно наш xview
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xview = new CanvasView(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(xview);
        xview.requestFocus();
    }
    
public boolean onTouchEvent(MotionEvent event)
{ //Обработка события нажания на экран
	boolean r = true;
	switch (xv.current_view)
	{
	case -1: //Игнорирование - управление программой запрещено
	r = false;
	break;
	case 0: //Передаём команду основному диалогу
	 xp.process_touch(event);
	 break;
	case 2: //Передаём команду меню диалога
	 xd.process_touch(event);
	 break;
	}
	if (event.getAction() == MotionEvent.ACTION_UP ||
		event.getAction() == MotionEvent.ACTION_CANCEL) {xv.ac = false;}
	else {xv.ac = true;} 
	//При выходе
	if (xv.ending) { finish(); r = false; }
	return r;
}

public boolean onKeyDown(int keyCode, KeyEvent event)
{ //Обработка нажатия кнопки
 Boolean r = true;
 if (event.getAction() == 1)
 {
  r = false;
  xv.keycode = 0;
  xv.keypressed = false;
  return r;
 }
 xv.keypressed = true;
 xv.keycode = keyCode;
 xv.ac = true;
 //Вызов процедур обработки кнопок в зависимости от теущего диалога
 switch (xv.current_view)
	{
	case -1: //Игнорирование - управление программой запрещено
	r = false;
	break;
	case 0: //Передаём команду основному диалогу
	 xp.process_key();
	 break;
	case 2: //Файловым диалогам
	 xd.process_key();
	}
 //При выходе
 if (xv.ending) { finish(); r = false; }
 return r;
}

public boolean onKeyUp(int keyCode, KeyEvent event)
{ //Обработка отпускания кнопки
	Boolean r = true;
	 xv.keypressed = false;
	 xv.keycode = 0;
	 switch (xv.current_view)
		{
		case -1: //Игнорирование - управление программой запрещено
		r = false;
		break;
		case 0: //Передаём команду основному диалогу
		 xp.key_up();
		 break;
		case 2:
		 xd.key_up();
		 break;
		}
	 xv.ac = false;
	 return r;
}

protected void onPause() {
   super.onPause();
 xv.focused = false;
}
    
public void onStart()
{
	super.onStart();
	//Выбор текущего диалога (-1 - управление программой запрещено)
	xv.current_view = -1;
	//Не знаю как можно узнать размер окна при запуске, поэтому для начала выясним разрешение экрана
    xv.swidth = this.getWindowManager().getDefaultDisplay().getWidth();
    xv.sheight = this.getWindowManager().getDefaultDisplay().getHeight();  
    //Получаем ресурсы (нельзя сделать в статичной функции, поэтому делаем это здесь)
    xv.res = getResources();
    //Инициализация вибры (тоже нельзя сделать в статичной функции)
  	xv.vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    //Запускаем статичную функцию загрузки программы
    main();
}

public static void main()
 {
	//Для начала создадим bitmap, на котором мы будем рисовать, используя поверхность cnv 
	xv.bitmap = Bitmap.createBitmap(xv.swidth, xv.sheight, Bitmap.Config.ARGB_8888);
    xv.cnv = new Canvas(xv.bitmap);
    //Создадим графический буфер для xview - сделано для снижения графических лагов, которые всё равно остаются :(
    xv.buf_bitmap = Bitmap.createBitmap(xv.swidth, xv.sheight, Bitmap.Config.ARGB_8888);
    xv.buf_cnv = new Canvas(xv.buf_bitmap);
    //Делаем "кисть" paint чёрной и полностью непрозрачной
    xv.paint.setARGB(255, 0, 0, 0);
    //Закрасим всё окно чёрным цветом
    xv.cnv.drawRect(0, 0, xv.swidth, xv.sheight, xv.paint);
    //Достаём из ресурсов логотип программы и рисуем его в центре окна
    xv.cnv.drawBitmap(BitmapFactory.decodeResource(xv.res, R.drawable.logo1),(xv.swidth / 2 - 120), (xv.sheight / 2 - 120), null);
    xv.buf_cnv.drawBitmap(xv.bitmap, 0, 0, null);
    //ЗАСТАВЛЯЕМ xview перерисоваться (дальше будем его об этом ПРОСИТЬ через repaint)
    xview.invalidate();
    xv.focused = true;
    //Создаём свой процесс (прерывание), чтобы система не думала, что наша программа "зависла"
    loading = new Thread(new Runnable() {
     public void run() {
     //Начало кода прерывания
     //Попытаемся поспать секунду, чтобы дать программе дозагрузиться (заработало только так - наверное система считает что это опасная задача)
     try { Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
     //Теперь наконец попробуем выяснить размер окна через xview - должно сработать
     if (xview.getHeight() > 0) xv.sheight = xview.getHeight();
     if (xview.getWidth() > 0) xv.swidth = xview.getWidth();
     //Зная размер окна, можно провести некоторые расчёты...
     xv.calc_param();
     xv.focused = true;
	 //Теперь можно начинать загрузку с визуальным сопровождением :)
     if (!xv.loaded) {
     Bitmap alogo = BitmapFactory.decodeResource(xv.res, R.drawable.logo2); 
	 xv.cnv.drawBitmap(alogo, xv.swidth - alogo.getWidth(), xv.sheight - alogo.getHeight() -1-xv.pbar_height, null);
	 //Заполнив буфер, просим xview перерисоваться
	 xv.buf_cnv.drawBitmap(xv.bitmap, 0, 0, null);
	 xview.post(repaint);
	 xv.paint.setARGB(255, 255, 255, 255);
	 xv.paint.setTextAlign(Align.RIGHT);
	 xv.cnv.drawText(xv.res.getString(R.string.app_name)+" "+xv.res.getString(R.string.ver)+" "+xv.version, xv.swidth, xv.text_size+1, xv.paint);
	 xv.s_pw = xv.res.getString(R.string.t_pleasewait);
	 xv.draw_load(5);
	 //Загружаем графику и масштабируем её под размер экрана
	 Bitmap temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_back);
	 int ih = xv.panel_height - 2;
	 int iw = (int) (ih * temp.getWidth() / temp.getHeight());
	 if (iw*5 > xv.swidth) 
	  { iw = (int) xv.swidth / 5; 
	  ih = (int) (iw * temp.getHeight() / temp.getWidth()); }
	 xv.b_back = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(10);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_back_pressed);
	 xv.b_back_p = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(15);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_forward);
	 xv.b_forw = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(20);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_forward_pressed);
	 xv.b_forw_p = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(25);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_pause);
	 xv.b_pause = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(30);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_pause_pressed);
	 xv.b_pause_p = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(35);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_play);
	 xv.b_play = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(40);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_play_pressed);
	 xv.b_play_p = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(45);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_up);
	 xv.b_up = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(50);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_up_pressed);
	 xv.b_up_p = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(55);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_down);
	 xv.b_down = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(60);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.button_down_pressed);
	 xv.b_down_p = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(65);
	 iw = xv.line_height;
	 ih = iw;
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_menu);
	 xv.i_menu = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(70);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_open);
	 xv.i_open = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(75);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_pause);
	 xv.i_pause = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(79);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_play);
	 xv.i_play = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(83);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_save);
	 xv.i_save = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(87);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_stop);
	 xv.i_stop = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(91);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_left_right);
	 xv.i_lr = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(95);
	 temp = BitmapFactory.decodeResource(xv.res, R.drawable.i_check);
	 xv.i_check = Bitmap.createScaledBitmap(temp, iw, ih, true);
	 xv.draw_load(100);
	 temp = null;
	 //Посчитаем положение кнопок на основном экране
	 iw = (int) xv.swidth / 5;
	 xv.b_pos[0] = (int) ((iw - xv.b_play.getWidth()) / 2);
	 xv.b_pos[1] = iw + xv.b_pos[0];
	 xv.b_pos[2] = 2*iw + xv.b_pos[0];
	 xv.b_pos[3] = 3*iw + xv.b_pos[0];
	 xv.b_pos[4] = 4*iw + xv.b_pos[0];
	 //Тоже для экрана диалога
	 iw = (int) xv.swidth / 3;
	 xd.b_pos[0] = (int) ((iw - xv.b_play.getWidth()) / 2);
	 xd.b_pos[1] = iw + xd.b_pos[0];
	 xd.b_pos[2] = 2*iw + xd.b_pos[0];
	 //Инициализация взаимодействия с файловой системой
	 xio.init_io();
	 //Загружаем плэйлист по-умолчанию и рисуем основной диалог
	 xio.load_default_playlist(); 
	 xv.loaded = true; }
	 //Устанавливаем текущий вид на основной диалог, разрешая управление программой
	 xv.current_view = 0;
	 //Рабочий цикл
	 while (!xv.ending)
	 {
	  while (xv.rc < 10) {
	   try { Thread.sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
	   xv.rc++;
	   }
	  if (!xv.focused) break;
	  if (!xv.ac) switch (xv.current_view)
	  {
	   case 0:
		   xp.draw_main_view();
		   break;
	   case 2:
		   xd.draw_dialog();
		   break;
	  }
	 }
	if (xv.ending)
	{
	 xv.current_view = -1;
	 xv.loaded = false;
	 xv.ending = false;
	//Рисуем окно при закрытии программы
	 xv.paint.setARGB(255, 0, 0, 0);
	 xv.buf_cnv.drawRect(0, 0, xv.swidth, xv.sheight, xv.paint);
	 xv.buf_cnv.drawBitmap(BitmapFactory.decodeResource(xv.res, R.drawable.logo1),(xv.swidth / 2 - 120), (xv.sheight / 2 - 120), null);
	 xv.paint.setTextAlign(Align.LEFT);
	 xv.paint.setARGB(255, 255, 255, 255);
	 xv.buf_cnv.drawText("Thanks for using", 1, xv.text_size+1, xv.paint);
	 xview.post(repaint);	    
	}
  }});
  loading.start();
 }

}

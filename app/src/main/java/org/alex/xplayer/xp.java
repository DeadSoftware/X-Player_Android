package org.alex.xplayer;

import android.graphics.Paint.Align;
import android.view.MotionEvent;

//Класс XPlayer
//Код основного диалога и плэера

public class xp {

	//Переменные плэера
	
	private static int MAX_LIST_SIZE = 1999;
	public static int play_current = 1;
	public static boolean play_paused;
	public static int plist_count = 0;
	public static int plist_vsf = -1; //View Staring From
	public static int plist_mpos = 0; //Marker position
	public static String plist[] = new String[MAX_LIST_SIZE];
	public static String plist_files[] = new String[MAX_LIST_SIZE];
	public static int b_pressed = 0;
	//Другие переменные
	private static int view_offset = 0;
	private static int touch_start = 0;
	private static int touch_sx;
	private static int touch_sy;
	private static int plist_svsf;
	
public static void draw_main_view()
{ //Рисование основного диалога
 xv.rc = 0; //Сброс счётчика до перерисовки
 xv.use_color(0);
 xv.cnv.drawRect(0, 0, xv.swidth, xv.sheight, xv.paint);
//Рисуем текст плэйлиста
 int view_end = plist_vsf + xv.line_count;
 if (view_end > plist_count - 1) view_end = plist_count-1;
 xv.paint.setTextAlign(Align.LEFT);
 for (int i = plist_vsf-1;i<=view_end;i++)
  {
   if (i < 0) continue;
   if (i == plist_mpos)
    {
     xv.use_color(7);
     //xv.paint.setAlpha(128);
     xv.cnv.drawRect(0, ((i-plist_vsf)*xv.line_height+view_offset), xv.swidth, ((i-plist_vsf+1)*xv.line_height+view_offset), xv.paint);
    }
   if (i == play_current) {xv.use_color(6);} else {xv.use_color(2);}
   xv.cnv.drawText(plist[i], 1, ((i-plist_vsf)*xv.line_height + xv.text_size + view_offset), xv.paint);
  }
 xv.paint.setAntiAlias(false);
 //Рисуем панели
 xv.use_color(1);
 xv.cnv.drawRect(0, 0, xv.swidth, xv.line_height, xv.paint);
 xv.cnv.drawRect((xv.swidth-xv.pbar_height-1), xv.line_height, xv.swidth, xv.sheight, xv.paint);
 xv.cnv.drawRect(0, (xv.sheight-xv.panel_height-xv.line_height-2), xv.swidth, xv.sheight, xv.paint);
 xv.use_color(4);
 xv.cnv.drawLine(0, (xv.line_height), (xv.swidth-xv.pbar_height-1), (xv.line_height), xv.paint);
 xv.cnv.drawLine(0, (xv.sheight-xv.panel_height-2), xv.swidth, (xv.sheight-xv.panel_height-2), xv.paint);
 xv.use_color(5);
 xv.cnv.drawLine(0, (xv.sheight-xv.panel_height-1), xv.swidth, (xv.sheight-xv.panel_height-1), xv.paint);
 xv.cnv.drawLine(0, (xv.sheight-xv.panel_height-xv.line_height-2), (xv.swidth-xv.pbar_height-1), (xv.sheight-xv.panel_height-xv.line_height-2), xv.paint);
 xv.cnv.drawLine((xv.swidth-xv.pbar_height-2), (xv.line_height+1), (xv.swidth-xv.pbar_height-2), (xv.sheight-xv.panel_height-xv.line_height-2), xv.paint);
 xv.paint.setAntiAlias(true);
 //Рисуем кнопки
 if (b_pressed != 1)
  xv.cnv.drawBitmap(xv.b_back, xv.b_pos[0], (xv.sheight - xv.panel_height + 1), xv.paint);
   else xv.cnv.drawBitmap(xv.b_back_p, xv.b_pos[0], (xv.sheight - xv.panel_height + 1), xv.paint);
 if (b_pressed != 2)
	  xv.cnv.drawBitmap(xv.b_forw, xv.b_pos[1], (xv.sheight - xv.panel_height + 1), xv.paint);
	   else xv.cnv.drawBitmap(xv.b_forw_p, xv.b_pos[1], (xv.sheight - xv.panel_height + 1), xv.paint);
 if (play_paused || play_current == -1)
  {
 if (b_pressed != 3)
	  xv.cnv.drawBitmap(xv.b_play, xv.b_pos[2], (xv.sheight - xv.panel_height + 1), xv.paint);
	   else xv.cnv.drawBitmap(xv.b_play_p, xv.b_pos[2], (xv.sheight - xv.panel_height + 1), xv.paint);
  } else
  {
	  if (b_pressed != 3)
		  xv.cnv.drawBitmap(xv.b_pause, xv.b_pos[2], (xv.sheight - xv.panel_height + 1), xv.paint);
		   else xv.cnv.drawBitmap(xv.b_pause_p, xv.b_pos[2], (xv.sheight - xv.panel_height + 1), xv.paint);
  }
 if (b_pressed != 4)
	  xv.cnv.drawBitmap(xv.b_down, xv.b_pos[3], (xv.sheight - xv.panel_height + 1), xv.paint);
	   else xv.cnv.drawBitmap(xv.b_down_p, xv.b_pos[3], (xv.sheight - xv.panel_height + 1), xv.paint);
 if (b_pressed != 5)
	  xv.cnv.drawBitmap(xv.b_up, xv.b_pos[4], (xv.sheight - xv.panel_height + 1), xv.paint);
	   else xv.cnv.drawBitmap(xv.b_up_p, xv.b_pos[4], (xv.sheight - xv.panel_height + 1), xv.paint);
 //Рисуем иконку состояния плэера
 if (play_current < 0)
  { xv.cnv.drawBitmap(xv.i_stop,0,0,xv.paint); }
   else
   { if (play_paused) { xv.cnv.drawBitmap(xv.i_pause, 0, 0, xv.paint); }
     else { xv.cnv.drawBitmap(xv.i_play, 0, 0, xv.paint); } }	
 xv.buf_cnv.drawBitmap(xv.bitmap, 0, 0, null);
 Xplay.xview.post(Xplay.repaint); 
}

public static void process_touch(MotionEvent event)
{ //Обработка нажания на экран
 int act = event.getAction();
 if (act == MotionEvent.ACTION_UP || act == MotionEvent.ACTION_CANCEL) 
  {
   b_pressed = 0;
   view_offset = 0;
   if (plist_vsf < -1) plist_vsf = -1;
   if (plist_vsf > plist_count-2) plist_vsf = plist_count - 2;
   touch_start = 0;
   touch_sx = 0;
   touch_sy = 0;
  }
 if (act == MotionEvent.ACTION_DOWN)
 { //Если нажали, то определяем куда
  if (event.getY() >= (xv.sheight - xv.panel_height))
  { //Если нажали на панель кнопок
   touch_start = 1;
  } else
   if (event.getY() >= (xv.sheight - xv.panel_height - xv.line_height))
   { //Если на панель прокрутки
    touch_start = 2;
   } else
    if (event.getX() < (xv.swidth * 0.80))
    { //На окно плейлиста
     touch_start = 3;
     plist_svsf = plist_vsf;
     //На полоску быстрой навигации
    } else { touch_start = 4; }
  //Запомним начальную точку
  touch_sx = (int) event.getX();
  touch_sy = (int) event.getY();
  //Выполняем действия при нажатии
  if (touch_start == 1) b_pressed = (int) (event.getX() * 5 / xv.swidth) + 1;
  if (touch_start == 3)
  {
   int mpos = (int) ((touch_sy - xv.text_size / 2)/xv.line_height - 1 + plist_vsf);
   if (plist_mpos == mpos) play_current = mpos;
   if (mpos >= 0 && mpos < plist_count) plist_mpos = mpos;
  }  
 }
 if (act == MotionEvent.ACTION_MOVE)
 { //Выполяется при движении...
  if (touch_start == 3)
  {//... по окну плейлиста
   int dy = (int) event.getY() - touch_sy;
   plist_vsf = plist_svsf - dy / xv.line_height; // div
   view_offset = dy % xv.line_height; // mod
   if (view_offset < 4 && view_offset > -4) view_offset = 0;
  }
 }
 draw_main_view();
}

public static void process_key()
{ //Обработка нажатых кнопок
 switch (xv.keycode)
 {
  case 21: //Влево
   b_pressed = 1;
   break;
  case 22: //Вправо
   b_pressed = 2;
   break;
  case 20: //Вниз
   plist_mpos = plist_mpos + 1;
   if (plist_mpos >= plist_count) plist_mpos = plist_count-1;
   plist_vsf = plist_mpos - xv.line_count / 2;
   if (plist_vsf < -1) plist_vsf = -1;
   break;
  case 19: //Вверх
   plist_mpos = plist_mpos - 1;
   if (plist_mpos < 0) plist_mpos = 0;
   plist_vsf = plist_mpos - xv.line_count / 2;
   if (plist_vsf < -1) plist_vsf = -1;
   break;
  case 23: //Центр
   play_current = plist_mpos;
   break;
  case 82: //Menu
   xd.show_mainmenu();
   break;
  case 4: //Back
   
   break;
 }
 draw_main_view();
}

public static void key_up()
{
 b_pressed = 0;
 draw_main_view();
}
	
}

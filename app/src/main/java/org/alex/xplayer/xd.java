package org.alex.xplayer;

import android.graphics.Paint.Align;
import android.view.MotionEvent;
import android.graphics.Bitmap;


//Класс диалогов
//Включает главное меню, файловые диалоги

public class xd {

public static byte dialog_type = 0;
public static String list[];
public static int list_count = 0;
public static int list_pos = 0;
public static int list_vsf = -1;
private static int list_svsf;
private static int view_offset = 0;
private static int touch_start = 0;
public static int b_pressed = 0;
private static int touch_sx;
private static int touch_sy;
public static Bitmap ico;
public static String header = "";
public static int b_pos[] = new int[3];

public static void show_mainmenu()
{
 dialog_type = 1;
 xv.current_view = 2;
 ico = xv.i_menu;
 header = xv.res.getString(R.string.t_mainmenu);
 list_count = 7;
 list_pos = 0;
 list_vsf = -1;
 list = new String[list_count];
 list[0] = xv.res.getString(R.string.mm_add);
 list[1] = xv.res.getString(R.string.mm_open);
 list[2] = xv.res.getString(R.string.mm_save);
 list[3] = xv.res.getString(R.string.mm_clear);
 list[4] = xv.res.getString(R.string.mm_setup);
 list[5] = xv.res.getString(R.string.mm_about);
 list[6] = xv.res.getString(R.string.mm_close);
 //xv.rc = 10; //Ставим счётчик до перерисовывания так что нужно перерисоваться пез промедлений
}

private static void process_menu_item(int index)
{
 switch (index)
 {
  case 0:
   show_add_file_dialog();
   break;
  case 3:
   xp.plist_count = 0;
   xv.current_view = 0;
   break;
  case 6:
   xv.ending = true;
   break;
 }
}

public static void show_add_file_dialog()
{
 dialog_type = 2;
 xv.current_view = 2;
 ico = xv.i_open;
 xv.current_view = 2;
 xio.d_read_curdir();
 list_vsf = -1;
}

public static void process_file_item(int index)
{ //Обработка нажатия на файл
 if (list_count < index) return;
 if (list[index].endsWith("/"))
  {xio.d_dirin(list[index]);}
 else
 {
  switch (dialog_type)
  {
  case 2: //Добавить файл в плейлист
   xv.vib.vibrate(100);
   break;
  }
 }
}

public static void draw_dialog()
{
 xv.rc = 0; //Сброс счётчика до перерисовки
 xv.use_color(0);
 xv.cnv.drawRect(0, 0, xv.swidth, xv.sheight, xv.paint);
 //Рисуем список диалога
 int view_end = list_vsf + xv.line_count;
 if (view_end > list_count - 1) view_end = list_count-1;
 xv.paint.setTextAlign(Align.LEFT);
 int tl = 1;
 if (dialog_type == 5) tl = xv.line_height+1;
 for (int i = list_vsf-1;i<=view_end;i++)
  {
   if (i < 0) continue;
   if (i == list_pos)
    {
     xv.use_color(7);
     xv.cnv.drawRect(0, ((i-list_vsf)*xv.line_height+view_offset), xv.swidth, ((i-list_vsf+1)*xv.line_height+view_offset), xv.paint);
    }
   xv.use_color(2);
   xv.cnv.drawText(list[i], tl, ((i-list_vsf)*xv.line_height + xv.text_size + view_offset), xv.paint);
  }
 xv.paint.setAntiAlias(false);
 //Рисуем панели
 xv.use_color(1);
 xv.cnv.drawRect(0, 0, xv.swidth, xv.line_height, xv.paint);
 xv.cnv.drawRect((xv.swidth-xv.pbar_height-1), xv.line_height, xv.swidth, xv.sheight, xv.paint);
 xv.cnv.drawRect(0, (xv.sheight-xv.panel_height-xv.line_height-2), xv.swidth, xv.sheight, xv.paint);
 xv.use_color(4);
 xv.cnv.drawLine(0, (xv.line_height), (xv.swidth-xv.pbar_height-1), (xv.line_height), xv.paint);
 xv.cnv.drawLine(0, (xv.sheight-xv.panel_height-2), xv.swidth, (xv.sheight-xv.panel_height-2), xv.paint);
 xv.use_color(5);
 xv.cnv.drawLine(0, (xv.sheight-xv.panel_height-1), xv.swidth, (xv.sheight-xv.panel_height-1), xv.paint);
 xv.cnv.drawLine(0, (xv.sheight-xv.panel_height-xv.line_height-2), (xv.swidth-xv.pbar_height-1), (xv.sheight-xv.panel_height-xv.line_height-2), xv.paint);
 xv.cnv.drawLine((xv.swidth-xv.pbar_height-2), (xv.line_height+1), (xv.swidth-xv.pbar_height-2), (xv.sheight-xv.panel_height-xv.line_height-2), xv.paint);
 xv.paint.setAntiAlias(true);
 //Рисуем кнопки
 if (b_pressed != 1)
  xv.cnv.drawBitmap(xv.b_back, b_pos[0], (xv.sheight - xv.panel_height + 1), xv.paint);
   else xv.cnv.drawBitmap(xv.b_back_p, b_pos[0], (xv.sheight - xv.panel_height + 1), xv.paint);
 if (b_pressed != 2)
  xv.cnv.drawBitmap(xv.b_play, b_pos[1], (xv.sheight - xv.panel_height + 1), xv.paint);
   else xv.cnv.drawBitmap(xv.b_play_p, b_pos[1], (xv.sheight - xv.panel_height + 1), xv.paint);
 if (b_pressed != 3)
  xv.cnv.drawBitmap(xv.b_forw, b_pos[2], (xv.sheight - xv.panel_height + 1), xv.paint);
   else xv.cnv.drawBitmap(xv.b_forw_p, b_pos[2], (xv.sheight - xv.panel_height + 1), xv.paint);
 //Рисуем всё остальное
 xv.cnv.drawBitmap(ico, 1, 1, xv.paint);
 xv.use_color(3);
 xv.cnv.drawText(header, xv.line_height, xv.text_size+1, xv.paint);
 //Вывод на экранный буфер, затем на экран
 xv.buf_cnv.drawBitmap(xv.bitmap, 0, 0, null);
 Xplay.xview.post(Xplay.repaint);
 //Вызов процедуры обработки нажатия на экранную кнопку
 if (b_pressed > 0) event_key();
}

public static void process_touch(MotionEvent event)
{ //Обработка нажания на экран
 int act = event.getAction();
 if (act == MotionEvent.ACTION_UP || act == MotionEvent.ACTION_CANCEL) 
  {
   b_pressed = 0;
   view_offset = 0;
   if (list_vsf < -1) list_vsf = -1;
   if (list_vsf > list_count-2) list_vsf = list_count - 2;
   touch_start = 0;
   touch_sx = 0;
   touch_sy = 0;
  }
 if (act == MotionEvent.ACTION_DOWN)
 { //Если нажали
  //Запомним начальную точку
  touch_sx = (int) event.getX();
  touch_sy = (int) event.getY();
  //Определяем куда
  if (event.getY() >= (xv.sheight - xv.panel_height))
  { //Если нажали на панель кнопок
   touch_start = 1;
  } else
   if (event.getY() >= (xv.sheight - xv.panel_height - xv.line_height))
   { //Если на панель прокрутки
    touch_start = 2;
   } else
    if (event.getX() < (xv.swidth * 0.80))
    { //На окно плейлиста
     touch_start = 3;
     list_svsf = list_vsf;
     //На полоску быстрой навигации
    } else { touch_start = 4; }
  //Выполняем действия при нажатии
  if (touch_start == 1) b_pressed = (int) (event.getX() * 3 / xv.swidth) + 1;
  if (touch_start == 3)
  {
   int mpos = (int) ((touch_sy - xv.text_size / 2)/xv.line_height - 1 + list_vsf);
   //Выполнение пункта диалога (соответствует нажатию на среднюю кнопку)
   if (list_pos == mpos) b_pressed = 2;
   if (mpos >= 0 && mpos < list_count) list_pos = mpos;
  }  
 }
 if (act == MotionEvent.ACTION_MOVE)
 { //Выполяется при движении...
  if (touch_start == 3)
  {//... по окну диалога
   int dy = (int) event.getY() - touch_sy;
   list_vsf = list_svsf - dy / xv.line_height; // div
   view_offset = dy % xv.line_height; // mod
   if (view_offset < 4 && view_offset > -4) view_offset = 0;
  }
 }
 draw_dialog();
}

public static void process_key()
{ //Обработка нажатых кнопок
 b_pressed = 0;
 switch (xv.keycode)
 {
  case 21: //Влево
   b_pressed = 1;
   break;
  case 22: //Вправо
   b_pressed = 3;
   break;
  case 20: //Вниз
   list_pos = list_pos + 1;
   if (list_pos >= list_count) list_pos = list_count-1;
   list_vsf = list_pos - xv.line_count / 2;
   if (list_vsf < -1) list_vsf = -1;
   break;
  case 19: //Вверх
   list_pos = list_pos - 1;
   if (list_pos < 0) list_pos = 0;
   list_vsf = list_pos - xv.line_count / 2;
   if (list_vsf < -1) list_vsf = -1;
   break;
  case 23: //Центр
   b_pressed = 2;
   break;
  case 82: //Menu
   xd.show_mainmenu();
   break;
  case 4: //Назад
   b_pressed = 1;
   break;
 }
 draw_dialog();
}

private static void event_key()
{ //Действия при нажатии на экранные кнопки
 switch (b_pressed)
 {
  case 1:
   if (dialog_type == 1) { xv.current_view = 0; break; }
   if (dialog_type >= 2 && dialog_type <= 4) xio.d_dirup();
   break;
  case 2:
   if (dialog_type == 1) { process_menu_item(list_pos); break; }
   if (dialog_type >= 2 && dialog_type <= 4) process_file_item(list_pos);
   break;
  case 3:
   if (dialog_type == 1) { process_menu_item(list_pos); break; }
   if (dialog_type >= 2 && dialog_type <= 4) process_file_item(list_pos);
   break;
 }
 b_pressed = 0;
}

public static void key_up()
{
 b_pressed = 0;
 draw_dialog();
}

}

package org.alex.xplayer;

/* Класс xv - XVariants :)
 * Определены переменные проекта,
 * небольшие общие процедуры
 */

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.os.Vibrator;

class xv {
    //Основные переменные
	public static Bitmap bitmap;
	public static Canvas cnv;
	public static Bitmap buf_bitmap;
	public static Canvas buf_cnv;
	public static Paint paint = new Paint();
	public static Resources res;
	public static String version = "0.0.4";
	public static String s_pw;
	public static int current_view = -1;
	public static boolean ending = false;
	public static boolean loaded = false;
	public static boolean ac;
	public static boolean focused;
	public static Vibrator vib;
	public static int rc = 10;
	//Разметка экрана
	public static int swidth;
	public static int sheight;
	public static int text_size;
	public static int pbar_height;
	public static int line_height;
	public static int panel_height;
	public static int line_count;
	//Переменные файловой системы
	public static boolean fs_sort = false;
	//Изображения
	public static Bitmap b_play;
	public static Bitmap b_play_p;
	public static Bitmap b_pause;
	public static Bitmap b_pause_p;
	public static Bitmap b_back_p;
	public static Bitmap b_back;
	public static Bitmap b_forw;
	public static Bitmap b_forw_p;
	public static Bitmap b_up;
	public static Bitmap b_up_p;
	public static Bitmap b_down;
	public static Bitmap b_down_p;
	public static Bitmap i_play;
	public static Bitmap i_stop;
	public static Bitmap i_pause;
	public static Bitmap i_open;
	public static Bitmap i_save;
	public static Bitmap i_menu;
	public static Bitmap i_lr;
	public static Bitmap i_check;
	//Цвета
	public static int colors[][] = new int[8][3];
	//Положение кнопок
	public static int b_pos[] = new int[5];
	//Переменные клавиатуры
	public static boolean keypressed = false;
	public static int keycode;
	
	//Установка цветов по-умолчанию
	private static void set_default_colors()
	{
	 colors[0][0] = 0; colors[0][1] = 0; colors[0][2] = 0;		//Фон (обычно чёрный)
	 colors[1][0] = 62; colors[1][1] = 46; colors[1][2] = 84;	//Фон панелей
	 colors[2][0] = 40; colors[2][1] = 254; colors[2][2] = 0;	//Основной текст
	 colors[3][0] = 190; colors[3][1] = 210; colors[3][2] = 250;//Заголовки
	 colors[4][0] = 42; colors[4][1] = 40; colors[4][2] = 62;	//Тень панелей
	 colors[5][0] = 100; colors[5][1] = 104; colors[5][2] = 116;//Свет панелей
	 colors[6][0] = 255; colors[6][1] = 255; colors[6][2] = 255;//Выделенный текст
	 colors[7][0] = 40; colors[7][1] = 80; colors[7][2] = 190;	//Маркер
	}
	
	//Выбор цвета
	public static void use_color(int index)
	{ paint.setARGB(255, colors[index][0], colors[index][1], colors[index][2]); }
	
	//Рисование линии загрузки внизу экрана (прогресс в процентах - от 0 до 100)
	public static void draw_load(int progress)
	{
	 if (pbar_height < 1) return;
	 paint.setARGB(255, 0, 0, 0);
	 cnv.drawRect(0, (sheight-pbar_height-1), swidth, sheight, paint);
	 paint.setARGB(255, 255, 255, 255);
	 if (progress > 0)
	  cnv.drawRect(0, sheight-pbar_height, (int) (swidth*progress/100), sheight, paint);
	 buf_cnv.drawBitmap(bitmap, 0, 0, null);
	 Xplay.xview.post(Xplay.repaint);
	}
	
	public static void calc_param()
	{ //Расчёт разметки экрана и немного инициализации
	  //Включаем сглаживание - хорошо для текста, не очень хорошо для графики
	  paint.setAntiAlias(true);
	  //Устанавливаем цвета
	  set_default_colors();
	  //Выполняем другие рассчёты
	  text_size = (int) sheight / 21;
	  if (text_size < 10) text_size = 10;
	  paint.setTextSize(text_size);
	  paint.setFakeBoldText(true);
	  pbar_height = (int) (sheight * 0.01);
	  if (pbar_height < 2) pbar_height = 2;
	  line_height = (int) (1.3 * text_size);
	  panel_height = (int) (line_height * 1.5);
	  line_count = (int) (sheight / line_height - 1);
	}
	
	public static void pwait()
	{ //Рисуем окошко "Пожалуйста, подождите..."
	 paint.setARGB(80, 0, 0, 0);
	 paint.setAntiAlias(false);
	 cnv.drawRect(0, 0, swidth, sheight, paint);
	 use_color(1);
	 cnv.drawRect(1,(sheight - panel_height) / 2, (sheight - 1), (sheight + panel_height) / 2, paint);
	 use_color(5);
	 cnv.drawLine(1, (sheight - panel_height) / 2, swidth - 2, (sheight - panel_height) / 2, paint);
	 cnv.drawLine(1, (sheight - panel_height) / 2, 1, (sheight + panel_height) / 2, paint);
	 use_color(4);
	 cnv.drawLine(1, (sheight + panel_height) / 2, swidth - 2, (sheight + panel_height) / 2, paint);
	 cnv.drawLine((sheight - 2), (sheight - panel_height) / 2, (sheight - 2), (sheight + panel_height) / 2, paint);
	 paint.setTextAlign(Align.CENTER);
	 use_color(3);
	 paint.setAntiAlias(true);
	 cnv.drawText(s_pw, swidth / 2, sheight / 2, paint);
	 buf_cnv.drawBitmap(bitmap, 0, 0, null);
	 Xplay.xview.post(Xplay.repaint);
	}
	
}
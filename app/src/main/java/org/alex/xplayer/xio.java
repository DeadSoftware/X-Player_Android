package org.alex.xplayer;

//Класс XPlayer Input-Output
//Содержит процедуры ввода/вывода

import java.io.*;
import android.os.Environment;

public class xio {
	
public static String curdir;
public static int ndirs;
public static int nfiles;

public static void init_io()
{
 curdir = Environment.getExternalStorageDirectory().getAbsolutePath();
 if (!curdir.endsWith("/")) curdir = curdir + "/";
}

public static void d_read_curdir()
{ //Чтение папки для диалога
 xv.pwait();
 xd.header = curdir;
 File f = new File(curdir);
 xd.list_count = f.listFiles().length;
 xd.list_pos = 0;
 xd.list_vsf = -1;
 if (xd.list_count < 1) return;
 xd.list = new String[xd.list_count];
 ndirs = 0; //Количество субдиректорий
 nfiles = 0; //Количество файлов
 //Перезаполняем список и (если нужно) сортируем пузырьком
 for (int n=0;n<xd.list_count;n++)
  {
   if (f.listFiles()[n].isDirectory())
   {
    xd.list[ndirs] = f.list()[n]+"/";
    ndirs++;
   }
  }
 for (int n=0;n<xd.list_count;n++)
 {
  if (!f.listFiles()[n].isDirectory())
  {
   xd.list[ndirs+nfiles] = f.list()[n];
   nfiles++;
  }
 }
 f = null;
 boolean d = true;
 String buf;
 if (xv.fs_sort) {
  //Сортируем папки
  while (d)
  {
  d = false;
  for (int n=0;n<ndirs-1;n++)
   {
    if (xd.list[n].compareToIgnoreCase(xd.list[n+1]) > 0)
     {
      buf = xd.list[n];
      xd.list[n] = xd.list[n+1];
      xd.list[n+1] = buf;
      d = true;
     }
   }
  }
  //Сортируем файлы
  d = true;
  while (d)
  {
  d = false;
  for (int n=ndirs;n<ndirs+nfiles-1;n++)
   {
    if (xd.list[n].compareToIgnoreCase(xd.list[n+1]) > 0)
     {
      buf = xd.list[n];
      xd.list[n] = xd.list[n+1];
      xd.list[n+1] = buf;
      d = true;
     }
   }
  }
  buf = null;
 }
 //xv.rc = 10; //Ставим счётчик до перерисовывания так что нужно перерисоваться пез промедлений
}

public static void d_dirup()
{
 if (curdir.length()>2)
 {
  int i = 0;
  for (int n=curdir.length()-2;n>0;n--)
   {
    if (curdir.charAt(n) == '/')
    {
     i = n;
     break;
    }
   }
  String dir = curdir.substring(i+1);
  curdir = curdir.substring(0, i+1);
  d_read_curdir();
  for (int n = 0; n<=ndirs;n++)
   {
	if (xd.list[n] == dir)
	{
	 xd.list_pos = n;
	 break;
	}
   }
 } else { xv.current_view = 0; }
}

public static void d_dirin(String name)
{
 if (name.endsWith("/"))
 {
  curdir = curdir.concat(name);
  d_read_curdir();
 }
}

public static void load_default_playlist()
{ //Загрузка плэйлиста по-умолчанию
 //Для тестирования заполняем плэйлист "Песнями" :)
 xp.plist_count = 16;
 xv.draw_load(1);
 try { Thread.sleep(300);} catch (InterruptedException e) {e.printStackTrace();}
 xp.plist[0] = "Песня 1";
 xp.plist[1] = "Песня 2";
 xp.plist[2] = "Песня 3";
 xp.plist[3] = "Песня 4";
 xv.draw_load(50);
 try { Thread.sleep(300);} catch (InterruptedException e) {e.printStackTrace();}
 xp.plist[4] = "Песня 5";
 xp.plist[5] = "Песня 6";
 xp.plist[6] = "Песня 7";
 xp.plist[7] = "Песня 8";
 xp.plist[8] = "Песня 9";
 xp.plist[9] = "Песня 10";
 xp.plist[10] = "Песня 11";
 xp.plist[11] = "Песня 12";
 xp.plist[12] = "Песня 13";
 xp.plist[13] = "Песня 14";
 xp.plist[14] = "Песня 15";
 xp.plist[15] = "Песня 16";
 xv.draw_load(100);
 try { Thread.sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
}
	
}

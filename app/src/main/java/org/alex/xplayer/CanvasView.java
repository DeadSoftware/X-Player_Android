package org.alex.xplayer;

/*
 * Описанние класса CanvasView
 * При перерисовке на его поверхности рисуется bitmap
 */

import android.view.View;
import android.content.Context;
import android.graphics.Canvas;

class CanvasView extends View {
	
public CanvasView(Context context) {
	  super(context);
	  setFocusable(true);  
  } 
  
  protected void onDraw(Canvas canvas)
  {
   canvas.drawBitmap(xv.buf_bitmap, 0, 0, xv.paint);
  }
}
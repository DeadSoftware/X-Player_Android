/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package org.alex.xplayer;

public final class R {
  public static final class drawable {
    public static final int button_back=0x7f010000;
    public static final int button_back_pressed=0x7f010001;
    public static final int button_down=0x7f010002;
    public static final int button_down_pressed=0x7f010003;
    public static final int button_forward=0x7f010004;
    public static final int button_forward_pressed=0x7f010005;
    public static final int button_pause=0x7f010006;
    public static final int button_pause_pressed=0x7f010007;
    public static final int button_play=0x7f010008;
    public static final int button_play_pressed=0x7f010009;
    public static final int button_up=0x7f01000a;
    public static final int button_up_pressed=0x7f01000b;
    public static final int i_check=0x7f01000c;
    public static final int i_left_right=0x7f01000d;
    public static final int i_menu=0x7f01000e;
    public static final int i_open=0x7f01000f;
    public static final int i_pause=0x7f010010;
    public static final int i_play=0x7f010011;
    public static final int i_save=0x7f010012;
    public static final int i_stop=0x7f010013;
    public static final int icon=0x7f010014;
    public static final int logo1=0x7f010015;
    public static final int logo2=0x7f010016;
  }
  public static final class layout {
    public static final int main=0x7f020000;
  }
  public static final class string {
    public static final int app_name=0x7f030000;
    public static final int mm_about=0x7f030001;
    public static final int mm_add=0x7f030002;
    public static final int mm_clear=0x7f030003;
    public static final int mm_close=0x7f030004;
    public static final int mm_open=0x7f030005;
    public static final int mm_save=0x7f030006;
    public static final int mm_setup=0x7f030007;
    public static final int t_mainmenu=0x7f030008;
    public static final int t_pleasewait=0x7f030009;
    public static final int ver=0x7f03000a;
  }
}